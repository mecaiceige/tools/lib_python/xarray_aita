.. xarray_aita documentation master file, created by
   sphinx-quickstart on Mon Aug  2 09:58:09 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to xarray_aita's documentation!
=======================================

``xarray_aita`` is a `xarray <http://xarray.pydata.org/en/stable/>`_ accessors to work on orientation map such as the one obtained using AITA.

In this documentation detail of the functions are given. For quick start script to use this accessors we recommend the `AITA Book <https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/documentations/AITA-book/docs/intro.html>`_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Installation
============

From repository
***************

.. code:: bash
    
    git clone https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/xarray_aita
    cd xarray_aita
    python -m pip install -r requirements.txt
    pip install -e .

Functions Overview
==================

.. toctree::
    :maxdepth: 0
    :numbered: 
    
    func_o

Dependancies
============

This ``xarray_aita`` module as few dependencies :

1. `xarray_uvecs <https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/lib_python/xarray_uvecs/>`_ for managing Unit VECtor that are Symmetric.


Contact
=======
:Author: Thomas Chauve
:Contact: thomas.chauve@univ-grenoble-alpes.fr

:Organization: `IGE <https://www.ige-grenoble.fr/>`_
:Status: This is a "work in progress"

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
