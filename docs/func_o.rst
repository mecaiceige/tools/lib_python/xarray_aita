Functions overview
==================

Loading data
************
.. automodule:: xarrayaita.loadData_aita
	:special-members:
	:members: 

aita xr.Dataset accessor
************************
.. automodule:: xarrayaita.aita
	:special-members:
	:members:
	:exclude-members: TJ_map, anisotropy_factors, closest_outG_value, closest_outTJ_value, craft, crop, dist2GB, dist2TJ_labels, dist2TJ_micro, dist2eachTJ, filter, fliplr, get_neighbours, interactive_crop, interactive_misorientation_profile, interactive_segmentation, mean_grain, plotBoundary, resize,rot180, rot90c,save

Geometric transformation
------------------------
.. automodule:: xarrayaita.aita_geom
	:special-members:
	:members:

Data processing
---------------
.. automodule:: xarrayaita.aita_processing
	:special-members:
	:members:

Data export
-----------
.. automodule:: xarrayaita.aita_export
	:special-members:
	:members:

Plot functions
--------------
.. automodule:: xarrayaita.aita_plot
	:special-members:
	:members:

Interactive functions for Jupyter Notebook
------------------------------------------
.. automodule:: xarrayaita.aita_interactive_nb
	:special-members:
	:members:
