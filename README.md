# Librairy xarray_aita

[![PyPI version](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/xarray_aita/-/blob/main/LICENSE)
[![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/xarray_aita/badges/main/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/xarray_aita/-/commits/main)
[![PyPI version](https://badge.fury.io/py/xarrayaita.svg)](https://badge.fury.io/py/xarrayaita)
[![Website shields.io](https://img.shields.io/website-up-down-green-red/http/shields.io.svg)](https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/lib_python/xarray_aita/)


This package is meant to be use with [xarray](https://github.com/pydata/xarray)
It is a wrapper for AITA output. Therefore It could easily be adapted for over data that are aita-like.

## Installation

### Form pypi

Run this command in your environment :

`pip install xarrayaita`

### From main

You should clone the environnement.

`git clone https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/xarray_aita`

Go in the folder.

```
cd xarray_aita
pip install poetry
poetry install
```
