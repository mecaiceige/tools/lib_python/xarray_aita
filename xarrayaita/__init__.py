#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .aita import aita
from .loadData_aita import *


__all__ = ['aita', 'loadData_aita']

__version__='0.3.3'
